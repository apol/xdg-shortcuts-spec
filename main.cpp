/*
 SPDX-FileCopyrightText: 2022 Aleix Pol Gonzalez <aleixpol@kde.org>

 SPDX-License-Identifier: BSD-3-Clause
 */

#include <gtk/gtk.h>
#include <QCommandLineParser>
#include <QCoreApplication>
#include <QDebug>
#include <QKeySequence>
#include <QRegularExpression>
#include <Qt>

#include <xkbcommon/xkbcommon.h>
#include <private/qxkbcommon_p.h>
#include <optional>

struct Modifier {
    const char *xkbModifier;
    Qt::KeyboardModifier qtModifier;
    GdkModifierType gtkModifier;
};

static const QHash<QString, Modifier> allowedModifiers = {
    { "SHIFT", { XKB_MOD_NAME_SHIFT, Qt::ShiftModifier, GDK_SHIFT_MASK } },
    { "CAPS", { XKB_MOD_NAME_CAPS, Qt::GroupSwitchModifier, GDK_LOCK_MASK } },
    { "CTRL", { XKB_MOD_NAME_CTRL, Qt::ControlModifier, GDK_CONTROL_MASK } },
    { "ALT", { XKB_MOD_NAME_ALT, Qt::AltModifier, GDK_ALT_MASK } },
    { "NUM", { XKB_MOD_NAME_NUM, Qt::KeypadModifier, GDK_HYPER_MASK } }, // GTK: Not very sure about this mask here
    { "LOGO", { XKB_MOD_NAME_LOGO, Qt::MetaModifier, GDK_META_MASK } },
};

QTextStream cout(stdout);

class Shortcut
{
public:
    bool parse(const QString &shortcutString) {
        QStringView remaining(shortcutString);
        while(!remaining.isEmpty()) {
            auto nextPlus = remaining.indexOf(QChar('+'));
            if (nextPlus == 0) { // ++ or ending with +
                qWarning() << "empty modifier";
                return false;
            } else if (nextPlus < 0) { // just the identifier left
                // The spec says that the string ends when all the spec'ed characters are over
                // Meaning "CTRL+a;Banana" would be an acceptable and parseable string
                const QRegularExpression rx("([a-zA-Z0-9_]+).*");
                QRegularExpressionMatch match = rx.match(remaining);

                m_identifier = xkb_keysym_from_name(match.captured(1).toUtf8().constData(), XKB_KEYSYM_CASE_INSENSITIVE);
                return m_identifier.has_value() && m_identifier != XKB_KEY_NoSymbol;
            } else { // A modifier
                auto modifier = remaining.left(nextPlus);
                if (!allowedModifiers.contains(modifier.toString())) {
                    qWarning() << "Unknown modifier" << modifier;
                    return false;
                }

                m_modifiers << modifier.toString();
                remaining = remaining.mid(nextPlus + 1);
            }
        }
        return false;
    }

    void describeXkb() const
    {
        cout << "xkb: ";
        for (const QString &modifier : m_modifiers) {
            cout << allowedModifiers[modifier].xkbModifier << '+';
        }

        QByteArray ba;
        ba.resize(128);
        int size = xkb_keysym_get_name(m_identifier.value(), ba.data(), ba.size());
        Q_ASSERT(size > 0 && size < ba.size());
        ba.resize(size);
        cout << ba << Qt::endl;
    }

    QKeySequence generateQt() const
    {
        uint keys = 0;
        for (const QString &modifier : m_modifiers) {
            keys |= allowedModifiers[modifier].qtModifier;
        }

        keys |= QXkbCommon::keysymToQtKey(m_identifier.value(), Qt::NoModifier, nullptr, XKB_KEYCODE_INVALID);
        return QKeySequence(keys);
    }

    GtkShortcutTrigger *generateGtk() {
        int modifiers = 0;
        for (const QString &modifier : m_modifiers) {
            modifiers |= allowedModifiers[modifier].gtkModifier;
        }
        return gtk_keyval_trigger_new(m_identifier.value(), (GdkModifierType) modifiers);
    }
private:
    QStringList m_modifiers;
    std::optional<xkb_keysym_t> m_identifier;
};

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    QCommandLineParser parser;
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument(app.tr("shortcut"), app.tr("Shortcut to translate"));
    parser.process(app);
    const QStringList shortcuts = parser.positionalArguments();

    for (const QString &shortcut : shortcuts) {
        cout << "Parsing" << shortcut << Qt::endl;
        Shortcut s;
        if (!s.parse(shortcut)) {
            qWarning() << "Failed to parse shortcut" << shortcut;
            continue;
        }

        s.describeXkb();

        cout << "QKeySequence: " << s.generateQt().toString() << Qt::endl;

        {
            g_autoptr(GtkShortcutTrigger) gtk = s.generateGtk();
            cout << "GtkShortcutTrigger: " << gtk_shortcut_trigger_to_string(gtk) << Qt::endl;
        }
        cout << Qt::endl;
    }

    return 0;
}
